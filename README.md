# Switch manifest for LineageOS Quorndog

### Issues
* HW overlays are forced off

### Patching
Basic:
* Repopick topics `icosa-bt-lineage-17.1`, `nvidia-shieldtech-q`, `nvidia-beyonder-q` off Lineage Gerrit
* Repopick commits `305978` and `309697` off Lineage Gerrit
* Apply all patches to their respective directories (from patches folder)

### Notes
* Use `foster_tab` if you want Nvidia games.
